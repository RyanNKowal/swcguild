
function rulePrompt(){
	var results = document.getElementById("div2");
	results.style.display = "none";
	alert("Welcome to Lucky Sevens! \n "
	+"The rules are as follows: \n "
	+"You win by rolling a 7! And if you win, $4 will be added to your total. \n "
	+"If you lose, $1 will be taken away from your total. \n "
	+"Good luck, and have fun! \n ");
};

function rollDice(){
		var results = document.getElementById("div2");
		results.style.display = "block";
		var userInput = document.getElementById("userInput").value;
		document.getElementById("a1").innerHTML = userInput;
		var totalRolls = 0;
		var bigWin = userInput;
	do{
		var die1 = Math.floor(Math.random()* 6) + 1;
		var die2 = Math.floor(Math.random()* 6) + 1;
		var dieSum = die1 + die2;
		totalRolls++;
		console.log("Current roll is "+totalRolls);
		document.getElementById("a2").innerHTML = totalRolls;
		
		if(dieSum == 7){
			userInput = (userInput - 1) + 5;
			console.log("You rolled a "+dieSum+"! You win $4. Total Pot = "+userInput);	
			if(userInput > bigWin){
				bigWin = userInput;
				document.getElementById("a3").innerHTML = bigWin;
				document.getElementById("a4").innerHTML = totalRolls;
			}
		}
		else{
			userInput -= 1;
			console.log("You rolled a "+dieSum+"! You lose $1. Total Pot = "+userInput);	
		}
	}while(userInput > 0);
					
	
	var gameOver = prompt("Game Over! You have run out of money! \n"
					+"If you would you like to play again, type 'Yes' now. \n"
					+"If you would like to quit, type 'No' now. \n");
					
		switch(gameOver){
			case "Yes":
				rollDice();
				break;
			case "No":
				alert("Thank you for playing! Have a great day!");
				break;
			default:
				gameOver = alert("Invalid response. Thank you for playing!");
				break;
		}
		
};





